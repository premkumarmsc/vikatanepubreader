//
//  UpdatesTableViewCell.h
//  UpdatesListView
//
//  Created by Tope on 10/11/2011.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LibraryCell : UITableViewCell
@property(nonatomic,retain )IBOutlet UIImageView *img;
@property(nonatomic,retain )IBOutlet UILabel *title;
@property(nonatomic,retain )IBOutlet UILabel *category;
@property(nonatomic,retain )IBOutlet UILabel *startfrom;

@property(nonatomic,retain )IBOutlet UILabel *distance;

@property (nonatomic,retain)IBOutlet UILabel *progress;
@property(nonatomic,retain )IBOutlet UIButton *deletebtn;
@property(nonatomic,retain)IBOutlet UIActivityIndicatorView *av;
@end
