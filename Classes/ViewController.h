//
//  ViewController.h
//  VikatanEpub
//
//  Created by ephronsystems on 6/18/13.
//  Copyright (c) 2013 EphronSystems. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ViewController : GAITrackedViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property(retain,nonatomic)IBOutlet UICollectionView *collectionView;
- (IBAction)Load:(id)sender;
@property (retain, nonatomic) IBOutlet UITableView *bookmarkTable;
- (IBAction)List:(id)sender;
- (IBAction)Grid:(id)sender;
- (IBAction)Edit:(id)sender;
- (IBAction)Store:(id)sender;


- (IBAction)Terms:(id)sender;
- (IBAction)FAQ:(id)sender;
- (IBAction)About:(id)sender;
- (IBAction)Contact:(id)sender;
- (IBAction)Referesh:(id)sender;

- (IBAction)CloseSearch:(id)sender;

@property(nonatomic,retain) IBOutlet UIActivityIndicatorView *Activity;
@property (retain, nonatomic) IBOutlet UIView *topView;
@property (retain, nonatomic) IBOutlet UIImageView *topImageView;
@property (retain, nonatomic) IBOutlet UIImageView *topLogo;
@property (retain, nonatomic) IBOutlet UIImageView *searchImage;
@property (retain, nonatomic) IBOutlet UIButton *storeButton;
@property (retain, nonatomic) IBOutlet UIButton *listButton;
@property (retain, nonatomic) IBOutlet UIButton *editButton;
@property (retain, nonatomic) IBOutlet UIButton *gridButton;
@property (retain, nonatomic) IBOutlet UITextField *searchTextField;
@property (retain, nonatomic) IBOutlet UIView *searchView;
@property (retain, nonatomic) IBOutlet UITableView *searchTable;
@property (retain, nonatomic)UIPopoverController *popoverController_bookmark;
@property (retain, nonatomic) IBOutlet UIView *bottomView;
@property (nonatomic,retain)NSString *comingFromSync;
@property (nonatomic,retain)NSString *syncBookID;
@property (nonatomic,retain)NSString *username;
@property (nonatomic,retain)NSString *password;
@property (retain, nonatomic)IBOutlet UIProgressView * threadProgressView;
@property (retain, nonatomic) IBOutlet UILabel *bottomLabel;

@end
