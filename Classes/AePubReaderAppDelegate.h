//
//  AePubReaderAppDelegate.h
//  AePubReader
//
//  Created by PREMKUMAR on 04/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Facebook.h"
@class GTMOAuth2Authentication;
//@class EPubViewController;
@class WebViewController;
@class ViewController;

@interface AePubReaderAppDelegate : NSObject <UIApplicationDelegate,FBSessionDelegate> {
    
    UIWindow *window;
        
    //EPubViewController *detailViewController;
    
    WebViewController *detailViewController;
    ViewController *detailViewController1;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, strong) Facebook *facebook;
//@property (nonatomic, retain) IBOutlet EPubViewController *detailViewController;
@property (nonatomic, retain) IBOutlet WebViewController *detailViewController;
@property (nonatomic, retain) IBOutlet ViewController *detailViewController1;
@end
