//
//  DetailViewController.m
//  AePubReader
//
//  Created by PREMKUMAR on 04/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "EPubViewController.h"
#import "ChapterListViewController.h"
#import "SearchResultsViewController.h"
#import "SearchResult.h"
#import "UIWebView+SearchWebView.h"
#import "Chapter.h"
#import "Dataware.h"
#import "Mes_UpdatesTableViewCell.h"
#import "ViewController.h"
#import "WebViewController.h"
#import <AnFengDe_EPUB_SDK/EPubSDKHeader.h>
@interface EPubViewController()


- (void) gotoNextSpine;
- (void) gotoPrevSpine;
- (void) gotoNextPage;
- (void) gotoPrevPage;

- (int) getGlobalPageCount;

- (void) gotoPageInCurrentSpine: (int)pageIndex;
- (void) updatePagination;

- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex;




@end

@implementation EPubViewController

@synthesize loadedEpub, toolbar, webView; 
@synthesize chapterListButton, decTextSizeButton, incTextSizeButton;
@synthesize currentPageLabel, pageSlider, searching;
@synthesize currentSearchResult;
@synthesize bookmarkTable,bookmarkView,viewbookmarkBtn;
@synthesize settingsPop,settingsView;
@synthesize settingBtn;
@synthesize alertView;
@synthesize epubName;
@synthesize comingView;
NSMutableArray *chapter_name_arr;
NSMutableArray *date_and_time_arr;
NSMutableArray *page_no_array;
NSMutableArray *chapter_no_array;
NSMutableArray *text_size_array;
NSMutableArray *mode_array;
NSMutableArray *book_name_array;
NSMutableArray *page_display_array;
NSMutableArray *orientation_Array;

int fullscree_check;
int landscape;

int settingscheck;
int book_delIndex;
UIAlertView *book_del_alert;

NSString *initialtext;

#pragma mark -


- (IBAction)settingsButton:(id)sender
{
    
    if (settingscheck==0) {
        settingscheck=1;
        settingsView.hidden=NO;
    }
    else
    {
        settingscheck=0;
        settingsView.hidden=YES;
    }
    
    
    
   
    

}



- (void) loadEpub:(NSURL*) epubURL{
    currentSpineIndex = 0;
    currentPageInSpineIndex = 0;
    pagesInCurrentSpineCount = 0;
    totalPagesCount = 0;
	searching = NO;
    epubLoaded = NO;
    self.loadedEpub = [[EPub alloc] initWithEPubPath:[epubURL path]];
    epubLoaded = YES;
    NSLog(@"loadEpub");
	[self updatePagination];
}

- (IBAction)viewBookmark:(id)sender
{
    
    
    
    
    settingsView.hidden=YES;
    settingscheck=0;
    
    chapter_name_arr=[[NSMutableArray alloc]init];
    date_and_time_arr=[[NSMutableArray alloc]init];
    page_no_array=[[NSMutableArray alloc]init];
    text_size_array=[[NSMutableArray alloc]init];
    chapter_no_array=[[NSMutableArray alloc]init];
    mode_array=[[NSMutableArray alloc]init];
    book_name_array=[[NSMutableArray alloc]init];
     page_display_array=[[NSMutableArray alloc]init];
    orientation_Array=[[NSMutableArray alloc]init];
    
    
    NSUserDefaults *passValues=[NSUserDefaults standardUserDefaults];
   
    
    NSString *book_id_str=[passValues objectForKey:@"BOOK_ID"];
    
    NSString *querystring=[NSString stringWithFormat:@"select book_name,chapter,bookmark,date,orientation from tb_bookmark where book_id=%@ ORDER BY id DESC",book_id_str];
                           
    const char *sqlvalue = [querystring UTF8String];
    
    NSLog(@"SQL VALUE:%s",sqlvalue);
    
    Dataware *dbsql=[[Dataware alloc]initDataware];
	sqlite3_stmt *sqlStmt=[dbsql OpenSQL:sqlvalue];
    
    
	if(sqlStmt != nil)
		while(sqlite3_step(sqlStmt) == SQLITE_ROW)
		{
            
            //NSLog(@"UU");
            
            NSString *chapter_name;
            @try {
                chapter_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 1)];
                
            }
            @catch (NSException *exception) {
                chapter_name=@"";
            }
            
			
            
            NSString *book_name=@"";
            
            @try {
                book_name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 0)];
            }
            @catch (NSException *exception) {
                book_name=@"";
            }
            
            NSString *orientation=@"";
            
            @try {
                orientation=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 4)];
            }
            @catch (NSException *exception) {
                orientation=@"";
            }
            
            NSString *bookmark;
            
            @try {
                bookmark=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 2)];
                
            }
            @catch (NSException *exception) {
                bookmark=@"";
            }
            
            
            NSString *date;
            
            
            
            @try {
                date=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 3)];
            }
            @catch (NSException *exception) {
                date=@"";
            }
            
            
            [chapter_name_arr addObject:chapter_name];
            
            [book_name_array addObject:book_name];
            [date_and_time_arr addObject:date];
            [orientation_Array addObject:orientation];
            
            
            NSArray *strings = [bookmark componentsSeparatedByString:@"|"];
            
            [page_no_array addObject:strings[0]];
            [chapter_no_array addObject:strings[1]];
            [text_size_array addObject:strings[2]];
            [mode_array addObject:strings[3]];
            [page_display_array addObject:strings[4]];
            
            
            
            //[Send_sub_arr addObject:subject];
            
            
            
            //NSLog(@"END");
            
            
            
            
        }
    
    NSLog(@"title_arr:%@",chapter_name_arr);
    NSLog(@"title_arr:%@",book_name_array);
    NSLog(@"title_arr:%@",date_and_time_arr);
    
    NSLog(@"title_arr:%@",page_no_array);
    NSLog(@"title_arr:%@",chapter_no_array);
    NSLog(@"title_arr:%@",text_size_array);
    NSLog(@"title_arr:%@",mode_array);
    
     NSLog(@"title_arr:%@",orientation_Array);
    
	[dbsql CloseSQL];

    
    
            
        [bookmarkTable reloadData];
		 bookmarkView.hidden=NO;
	

        
}
-(IBAction)bookMarkBack:(id)sender
{
     bookmarkView.hidden=YES;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return [chapter_name_arr count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 55;
    
}
- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    
    Mes_UpdatesTableViewCell * cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"Mes_UpdatesTableViewCell" owner:nil options:nil];
        
        for (UIView *view in views)
        {
            if([view isKindOfClass:[UITableViewCell class]])
            {
                cell = (Mes_UpdatesTableViewCell*)view;
                //cell.img=@"date.png";
                
            }
        }
    }
    
    
 
    
    [cell.btn1 setTag:indexPath.row];
    [cell.btn1 addTarget:self action:@selector(clickbuttonClicked:)
       forControlEvents:UIControlEventTouchDown];
    
    cell.seller.text=[NSString stringWithFormat:@"Chapter %@",chapter_name_arr[indexPath.row]];
   // cell.title.text=[NSString stringWithFormat:@"Page:%@",page_display_array[indexPath.row]];
    cell.startfrom.text=date_and_time_arr[indexPath.row];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return cell;
}


-(void)clickbuttonClicked:(UIButton*)button
{
  
    
    
    
    NSString *orientation_str=@"";
    
    
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (UIInterfaceOrientationIsPortrait(interfaceOrientation))
    {
        orientation_str=@"PORTRAIT";
    }
    else
    {
        orientation_str=@"LANDSCAPE";
        
        
    }
    
    
    if ([orientation_str isEqualToString:orientation_Array[(long int)[button tag]]])
    {
        
        
        if ([mode_array[(long int)[button tag]]isEqualToString:@"GRAY"]) {
            
            NSUserDefaults *userDefaults2 = [NSUserDefaults standardUserDefaults];
            
            [userDefaults2 setObject:@"GRAY" forKey:@"btnM1"];
            
            [userDefaults2 synchronize];
            
            [webView setOpaque:NO];
            //[webView setBackgroundColor:[UIColor colorWithRed:243.0/255.0 green:228.0/255.0  blue:171.0/255.0  alpha:1]];
            [webView setBackgroundColor:[UIColor colorWithRed:243.0/255.0 green:228.0/255.0  blue:171.0/255.0  alpha:1]];
            
            NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextFillColor= 'black'"];
            [webView stringByEvaluatingJavaScriptFromString:jsString];
            
            self.view.backgroundColor=[UIColor colorWithRed:243.0/255.0 green:228.0/255.0  blue:171.0/255.0  alpha:1];
        }
        else  if ([mode_array[(long int)[button tag]]isEqualToString:@"WHITE"])
        {
            
            NSUserDefaults *userDefaults2 = [NSUserDefaults standardUserDefaults];
            [userDefaults2 setObject:@"WHITE" forKey:@"btnM1"];
            [userDefaults2 synchronize];
            
            [webView setOpaque:NO];
            [webView setBackgroundColor:[UIColor whiteColor]];
            NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextFillColor= 'black'"];
            [webView stringByEvaluatingJavaScriptFromString:jsString];
            
            self.view.backgroundColor=[UIColor whiteColor];
            
        }
        else
        {
            
            NSUserDefaults *userDefaults2 = [NSUserDefaults standardUserDefaults];
            [userDefaults2 setObject:@"BLACK" forKey:@"btnM1"];
            [userDefaults2 synchronize];
            
            [webView setOpaque:NO];
            [webView setBackgroundColor:[UIColor blackColor]];
            NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextFillColor= 'white'"];
            [webView stringByEvaluatingJavaScriptFromString:jsString];
            
            self.view.backgroundColor=[UIColor blackColor];
            
        }
        
        currentTextSize=[text_size_array[(long int)[button tag]]intValue];
        
        
        NSLog(@"SPINE:%d",[chapter_no_array[(long int)[button tag]] intValue]-1);
        NSLog(@"PAGE:%d",[page_no_array[(long int)[button tag]] intValue]);
        
        // [self loadSpine:[chapter_no_array[indexPath.row] intValue]-1 atPageIndex:[page_no_array[indexPath.row]intValue]];
        
        
        [self loadSpine:[chapter_no_array[(long int)[button tag]] intValue]-1 atPageIndex:[page_no_array[(long int)[button tag]] intValue]];
        
        [self getGlobalPageValue];
        
        
        bookmarkView.hidden=YES;
        
    }
    else
    {
        NSString *displayStr=[NSString stringWithFormat:@"This Bookmark added as %@ mode, Please change to %@ mode and use this Bookmark.",orientation_Array[(long int)[button tag]],orientation_Array[(long int)[button tag]]];
        
        UIAlertView *alert_view=[[UIAlertView alloc]initWithTitle:@"Information" message:displayStr delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert_view show];
        [alert_view release];
        
    }
    bookmarkView.hidden=YES;

    
    
    /*
     
     firstalert=[[UIAlertView alloc]initWithTitle:@"Information" message:@"Do you want to delete this card?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
     
     [firstalert show];
     
     */
    
    
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *cardID=date_and_time_arr[indexPath.row];
    
    NSLog(@"UNIQUE:%@",cardID);
    
    
    
    NSLog(@"CONFIRM DELETE");
    
    book_delIndex=indexPath.row;
    
    book_del_alert = [[[UIAlertView alloc] initWithTitle:@"Really Delete?"
                                               message:@"Do you really want to remove this Bookmark?"
                                              delegate:self
                                     cancelButtonTitle:@"Cancel"
                                     otherButtonTitles:@"Yes",nil] autorelease];
    
    [book_del_alert show];
    
    
}


- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    
    
    if (alertView==book_del_alert)
    {
        NSLog(@"DEL:%@",date_and_time_arr[book_delIndex]);
        
        //NSLog(@"Sele:%@",[abc objectAtIndex:0]);
        
        NSString *delIDStr=[NSString stringWithFormat:@"%@",date_and_time_arr[book_delIndex]];
        if (buttonIndex==1)
        {
            
            NSLog(@"DEL ID:%@",delIDStr);
            
            
            NSUserDefaults *passValues=[NSUserDefaults standardUserDefaults];
            
            
            NSString *book_id_str=[passValues objectForKey:@"BOOK_ID"];

                       
            Dataware *db1=[[Dataware alloc]initDataware];
            NSString *strr=[NSString stringWithFormat:@"DELETE from tb_bookmark where book_id=? and date=?"];
            sqlite3_stmt *stmt=[db1 OpenSQL:[strr UTF8String]];
            if(stmt != nil)
            {
                //sqlite3_bind_int(stmt,1,mobile_str);
                sqlite3_bind_text(stmt,1,[book_id_str  UTF8String],-1,SQLITE_TRANSIENT);
                 sqlite3_bind_text(stmt,2,[delIDStr  UTF8String],-1,SQLITE_TRANSIENT);
                
                sqlite3_step(stmt);sqlite3_reset(stmt);
                
            }
            
            [db1 CloseSQL];
            
            
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Success !"
                                                                message:@"Bookmark Removed Successfully" delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            [alertView release];

            
            
             bookmarkView.hidden=YES;
            
        }
        else
        {
            
        }
    }
    
}


-(void)getGlobalPageValue
{
    NSUserDefaults *checkValue=[NSUserDefaults standardUserDefaults];
    
    currentTextSize=[[checkValue objectForKey:@"GLOBAL_SIZE"]intValue];
    
    NSLog(@"GLOBAL SIZE:%d",currentTextSize);
    
    
   // [checkValue setObject:currentTextSize forKey:@"GLOBAL_SIZE"];
}



/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */



-(IBAction)bookMark:(id)sender
{
    
    settingsView.hidden=YES;
    settingscheck=0;
  
    
    
    
    NSLog(@"GLOBAL:%d",[self getGlobalPageCount]);
    NSLog(@"Current text Size:%d",currentTextSize);
    NSString *mode;
    NSUserDefaults *menuUserDefaults = [NSUserDefaults standardUserDefaults];
    
       NSString *mode_str=[menuUserDefaults objectForKey:@"btnM1"];
    
    
    if([mode_str isEqualToString:@"BLACK"])
         mode=@"BLACK";
    else if([mode_str isEqualToString:@"WHITE"])
        mode=@"WHITE";
    else
        mode=@"GRAY";
    
       
    NSLog(@"CURRENT SPYNE INDEX:%d",currentSpineIndex+1);
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    
    NSArray *array=[check objectForKey:@"CHAPTERS"];
    
   // NSLog(@"ARRAY:%@",array[currentSpineIndex]);
    
    
    NSUserDefaults *passValues=[NSUserDefaults standardUserDefaults];
    //[passValues setObject:idArray[indexPath.row] forKey:@"BOOK_ID"];
    // [passValues setObject:epubArray[indexPath.row] forKey:@"BOOK_EPUB"];
    // [passValues setObject:textArray[indexPath.row] forKey:@"BOOK_NAME"];
    
   
    
    NSString *book_id=[passValues objectForKey:@"BOOK_ID"];
    NSString *book_name=[passValues objectForKey:@"BOOK_NAME"];
    
    
    
    NSString *orientation_str=@"";
    
    
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (UIInterfaceOrientationIsPortrait(interfaceOrientation))
    {
        orientation_str=@"PORTRAIT";
    }
    else
    {
        orientation_str=@"LANDSCAPE";
        
       
    }
    
    NSLog(@"ORIENTATION STR:%@",orientation_str);
    
    
    Dataware *dd=[[Dataware alloc]initDataware];
    
    NSString *sstr=@"";
    
    sstr=@"INSERT INTO tb_bookmark(book_name,chapter,bookmark,date,book_id,orientation) VALUES(?,?,?,?,?,?)";
    
    //NSString *book_name=@"test";
    NSString *chapter;
    @try {
       chapter =array[currentSpineIndex];
    }
    @catch (NSException *exception) {
        chapter=[NSString stringWithFormat:@"Chapter:%d",currentSpineIndex+1];
    }
   
   
    
     NSString *loc = [NSString stringWithFormat:@"%d-%d-%d",currentSpineIndex,currentPageInSpineIndex,currentTextSize];
    
    NSString *bookmark=[NSString stringWithFormat:@"%d|%d|%d|%@|%d",currentPageInSpineIndex,currentSpineIndex+1,currentTextSize,mode,[self getGlobalPageCount]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy hh:mm:ss:a"];
    NSString *strDate = [dateFormatter stringFromDate:[NSDate date]];
    
                 
    sqlite3_stmt *stmt=[dd OpenSQL:[sstr UTF8String]];
    if (stmt!=nil)
    {
        sqlite3_bind_text(stmt,1,[book_name UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(stmt,2, [chapter UTF8String],-1 ,SQLITE_TRANSIENT);
        sqlite3_bind_text(stmt,3, [bookmark UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(stmt,4, [strDate UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(stmt,5, [book_id UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(stmt,6, [orientation_str UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_step(stmt);
    }
    sqlite3_finalize(stmt);
        
    
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Success !"
                                                        message:@"Bookmark Added Successfully" delegate:self
                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
    [alertView release];
    
    
    
    
    
     

        
}

- (void)webViewDidStartLoad:(UIWebView *)webView1 
{
   //NSLog(@"Request Url:%@",request.URL.absoluteURL);
    
}

- (void) chapterDidFinishLoad:(Chapter *)chapter
{
    totalPagesCount+=chapter.pageCount;

	if(chapter.chapterIndex + 1 < [loadedEpub.spineArray count]){
		[[loadedEpub.spineArray objectAtIndex:chapter.chapterIndex+1] setDelegate:self];
        
        
       // NSLog(@"HELLO CHAPTER:%@",[loadedEpub.spineArray objectAtIndex:chapter.chapterIndex+1]);
        
		[[loadedEpub.spineArray objectAtIndex:chapter.chapterIndex+1] loadChapterWithWindowSize:webView.bounds fontPercentSize:currentTextSize];
		[currentPageLabel setText:[NSString stringWithFormat:@"Loading... /%d", totalPagesCount]];
	} else
    
    {
        
        [alertView dismissWithClickedButtonIndex:0 animated:YES];
        
        
		[currentPageLabel setText:[NSString stringWithFormat:@"%d/%d",[self getGlobalPageCount], totalPagesCount]];
		[pageSlider setValue:(float)100*(float)[self getGlobalPageCount]/(float)totalPagesCount animated:YES];
		paginating = NO;
		NSLog(@"Pagination Ended!:%d",[self getGlobalPageCount]);
	}
}

- (int) getGlobalPageCount{
	int pageCount = 0;
	for(int i=0; i<currentSpineIndex; i++)
    {
		pageCount+= [[loadedEpub.spineArray objectAtIndex:i] pageCount]; 
	}
	pageCount+=currentPageInSpineIndex+1;
	return pageCount;
}

- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex
{
    
    NSLog(@"SPINE INDEX:%d",spineIndex);
     NSLog(@"Page INDEX:%d",pageIndex);
    
    
	[self loadSpine:spineIndex atPageIndex:pageIndex highlightSearchResult:nil];
}

- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex highlightSearchResult:(SearchResult*)theResult{
	
    
    NSLog(@"SPINE:%d",spineIndex);
    NSLog(@"PAGE:%d",pageIndex);
    
	webView.hidden = YES;
	
	self.currentSearchResult = theResult;

	[chaptersPopover dismissPopoverAnimated:YES];
	[searchResultsPopover dismissPopoverAnimated:YES];
	
	NSURL* url = [NSURL fileURLWithPath:[[loadedEpub.spineArray objectAtIndex:spineIndex] spinePath]];
    
    NSLog(@"URL::%@",url);
        
	[webView loadRequest:[NSURLRequest requestWithURL:url]];
	currentPageInSpineIndex = pageIndex;
	currentSpineIndex = spineIndex;
    
    
    NSLog(@"CURRENT:%d",currentPageInSpineIndex);
     NSLog(@"CURRENT:%d",currentSpineIndex);
    
	if(!paginating)
    {
        NSLog(@"NOT PAG");
        
		[currentPageLabel setText:[NSString stringWithFormat:@"%d/%d",[self getGlobalPageCount], totalPagesCount]];
		[pageSlider setValue:(float)100*(float)[self getGlobalPageCount]/(float)totalPagesCount animated:YES];	
	}
    else
    {
        NSLog(@"APGI");
    }
}

- (void) gotoPageInCurrentSpine:(int)pageIndex{
	if(pageIndex>=pagesInCurrentSpineCount){
		pageIndex = pagesInCurrentSpineCount - 1;
		currentPageInSpineIndex = pagesInCurrentSpineCount - 1;
	}
	
	float pageOffset = pageIndex*webView.bounds.size.width;

	NSString* goToOffsetFunc = [NSString stringWithFormat:@" function pageScroll(xOffset){ window.scroll(xOffset,0); } "];
	NSString* goTo =[NSString stringWithFormat:@"pageScroll(%f)", pageOffset];
	
	[webView stringByEvaluatingJavaScriptFromString:goToOffsetFunc];
	[webView stringByEvaluatingJavaScriptFromString:goTo];
	
    
    NSLog(@"Global:%d",[self getGlobalPageCount]);
    
	if(!paginating){
		[currentPageLabel setText:[NSString stringWithFormat:@"%d/%d",[self getGlobalPageCount], totalPagesCount]];
		[pageSlider setValue:(float)100*(float)[self getGlobalPageCount]/(float)totalPagesCount animated:YES];	
	}
	
	webView.hidden = NO;
	
}

-(IBAction) segmentedControlIndexChanged
{
    settingsView.hidden=YES;
    settingscheck=0;
    NSUserDefaults *page=[NSUserDefaults standardUserDefaults];
    
    
    switch (self.segmentedControl.selectedSegmentIndex)
    {
        case 0:
            //self.segmentLabel.text =@&quot;Segment 1 selected.&quot;;
            NSLog(@"OOO");
            NSString *insertRule1 = [NSString stringWithFormat:@"addCSSRule('html', 'padding: 0px; height: %fpx; -webkit-column-gap: 0px; -webkit-column-width: %fpx;')", webView.frame.size.height, webView.frame.size.width];
            [webView stringByEvaluatingJavaScriptFromString:insertRule1];
            [page setObject:@"SINGLE" forKey:@"LAYOUT"];
            
            break;
        case 1:
        {
            //self.segmentLabel.text =@&quot;Segment 2 selected.&quot;;
             NSString *insertRule1 = [NSString stringWithFormat:@"addCSSRule('html', 'padding: 0px; height: %fpx; -webkit-column-gap: 0px; -webkit-column-width: %fpx;')", webView.frame.size.height, webView.frame.size.width / 2];
            [webView stringByEvaluatingJavaScriptFromString:insertRule1];
            [page setObject:@"DOUBLE" forKey:@"LAYOUT"];
            NSLog(@"1111");
        }
            break;
            
        default:
            break;
    }
    
}

- (void) gotoNextSpine
{
	if(!paginating)
    {
         NSLog(@"MNEKKI");
		if(currentSpineIndex+1<[loadedEpub.spineArray count])
        {
			[self loadSpine:++currentSpineIndex atPageIndex:0];
		}
        
        
	}
    else
    {
        NSLog(@"HEKKI");
        
        if(currentSpineIndex+1<[loadedEpub.spineArray count])
        {
			[self loadSpine:++currentSpineIndex atPageIndex:0];
		}
        
        
    }
}

- (void) gotoPrevSpine
{
	if(!paginating)
    {
		if(currentSpineIndex-1>=0){
			[self loadSpine:--currentSpineIndex atPageIndex:0];
		}
      
	}
}

- (void) gotoNextPage
{
    
    //    CATransition *animation = [CATransition animation];
    //    [animation setDelegate:self];
    //    [animation setDuration:0.2f];
    //    animation.startProgress = 0.5;
    //    animation.endProgress   = 5;
    //    [animation setTimingFunction:UIViewAnimationCurveEaseInOut];
    //    //[animation setType:@"pageCurl"];
    //
    //    //[animation setType:kcat];
    //    [animation setSubtype:kCATransitionMoveIn];
    //
    //    [animation setRemovedOnCompletion:NO];
    //    [animation setFillMode: @"extended"];
    //    [animation setRemovedOnCompletion: NO];
    //    [[webView layer] addAnimation:animation forKey:@"WebPageCurl"];
    
    NSLog(@"ABC");
    
	if(!paginating)
    {
        NSLog(@"NOT paginating");
        
        
        
        
        
        
		if(currentPageInSpineIndex+1<pagesInCurrentSpineCount)
        {
            NSLog(@"YES");
            
			[self gotoPageInCurrentSpine:++currentPageInSpineIndex];
		} else
        {
            NSLog(@"NO");
            
			[self gotoNextSpine];
		}
        
        
        NSLog(@"PAGE:%d",currentPageInSpineIndex);
        
        
        
        NSLog(@"GLOBAl:%d/%d",[self getGlobalPageCount],totalPagesCount);
        
        
        if ([self getGlobalPageCount]>=totalPagesCount)
        {
            NSLog(@"ENTER");
            
          
            
            
            [currentPageLabel setText:[NSString stringWithFormat:@"%d/%d",totalPagesCount, totalPagesCount]];
        }
        else
        {
            CATransition *transition = [CATransition animation];
            [transition setDelegate:self];
            [transition setDuration:0.5f];
            [transition setType:kCATransitionPush];
            [transition setSubtype:kCATransitionFromRight];
            [self.webView.layer addAnimation:transition forKey:@"PageAnim"];
        }
        
	}
    else
    {
        NSLog(@"PAGINATINg paginating");
        
		if(currentPageInSpineIndex+1<pagesInCurrentSpineCount)
        {
            NSLog(@"YES");
            
			[self gotoPageInCurrentSpine:++currentPageInSpineIndex];
		} else
        {
            NSLog(@"NO");
            
			[self gotoNextSpine];
		}
        
        if ([self getGlobalPageCount]>=totalPagesCount)
        {
            [currentPageLabel setText:[NSString stringWithFormat:@"%d/%d",totalPagesCount, totalPagesCount]];
        }
        else
        {
            
            CATransition *transition = [CATransition animation];
            [transition setDelegate:self];
            [transition setDuration:0.5f];
            [transition setType:kCATransitionPush];
            [transition setSubtype:kCATransitionFromRight];
            [self.webView.layer addAnimation:transition forKey:@"PageAnim"];
        }
        
        
        
    }
}

- (void) gotoPrevPage
{
    
    
    
	if (!paginating)
    {
		if(currentPageInSpineIndex-1>=0)
        {
            
            // NSLog(@"Hello YES");
            
			[self gotoPageInCurrentSpine:--currentPageInSpineIndex];
            
            
            CATransition *transition = [CATransition animation];
            [transition setDelegate:self];
            [transition setDuration:0.5f];
            [transition setType:kCATransitionPush];
            [transition setSubtype:kCATransitionFromLeft];
            [self.webView.layer addAnimation:transition forKey:@"PageAnim"];
		}
        else
        {
            // NSLog(@"Hello NO");
            
			if(currentSpineIndex!=0)
            {
                
                //  NSLog(@"YES YES");
                
				int targetPage = [[loadedEpub.spineArray objectAtIndex:(currentSpineIndex-1)] pageCount];
				[self loadSpine:--currentSpineIndex atPageIndex:targetPage-1];
                
                
                
                CATransition *transition = [CATransition animation];
                [transition setDelegate:self];
                [transition setDuration:0.5f];
                [transition setType:kCATransitionPush];
                [transition setSubtype:kCATransitionFromLeft];
                [self.webView.layer addAnimation:transition forKey:@"PageAnim"];
                
			}
            else
            {
                // NSLog(@"NO NO");
                
            }
		}
        
        
        
        
        
	}
    else
    {
        if(currentPageInSpineIndex-1>=0)
        {
			[self gotoPageInCurrentSpine:--currentPageInSpineIndex];
		}
        else
        {
			if(currentSpineIndex!=0)
            {
				int targetPage = [[loadedEpub.spineArray objectAtIndex:(currentSpineIndex-1)] pageCount];
				[self loadSpine:--currentSpineIndex atPageIndex:targetPage-1];
                
                
                
                
			}
		}
        
        CATransition *transition = [CATransition animation];
        [transition setDelegate:self];
        [transition setDuration:0.5f];
        [transition setType:kCATransitionPush];
        [transition setSubtype:kCATransitionFromLeft];
        [self.webView.layer addAnimation:transition forKey:@"PageAnim"];
        
    }
    
    
    
    if ([self getGlobalPageCount]>=totalPagesCount)
    {
        [currentPageLabel setText:[NSString stringWithFormat:@"%d/%d",totalPagesCount, totalPagesCount]];
    }
}


/*
GLOBAL:91
2013-06-19 11:21:44.123 AePubReader[40246:11303] Current text Size:100
2013-06-19 11:21:44.124 AePubReader[40246:11303] NIGHT MODE
2013-06-19 11:21:44.124 AePubReader[40246:11303] CURRENT SPYNE INDEX:11
2013-06-19 11:21:44.128 AePubReader[40246:11303] Content:
*/


- (IBAction) increaseTextSizeClicked:(id)sender
{
    [settingsPop dismissPopoverAnimated:YES];
    
    settingsView.hidden=YES;
    settingscheck=0;
    
    /*
    alertView = [[UIAlertView alloc] initWithTitle:@"Updating..."
                                           message:@"\n"
                                          delegate:self
                                 cancelButtonTitle:nil
                                 otherButtonTitles:nil];
    
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.center = CGPointMake(139.5, 75.5); // .5 so it doesn't blur
    [alertView addSubview:spinner];
    [spinner startAnimating];
    [alertView show];
    */
	if(!paginating)
    {
		if(currentTextSize+25<=200)
        {
			currentTextSize+=25;
			[self updatePagination];
			if(currentTextSize == 200){
				[incTextSizeButton setEnabled:NO];
			}
			[decTextSizeButton setEnabled:YES];
		}
	}
    else
    {
        if(currentTextSize+25<=200){
			currentTextSize+=25;
			[self updatePagination];
			if(currentTextSize == 200)
            {
				[incTextSizeButton setEnabled:NO];
			}
			[decTextSizeButton setEnabled:YES];
		}
    }
}
- (IBAction) decreaseTextSizeClicked:(id)sender
{
    settingsView.hidden=YES;
    settingscheck=0;
    [settingsPop dismissPopoverAnimated:YES];
    
    /*
    alertView = [[UIAlertView alloc] initWithTitle:@"Updating..."
                                           message:@"\n"
                                          delegate:self
                                 cancelButtonTitle:nil
                                 otherButtonTitles:nil];
    
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.center = CGPointMake(139.5, 75.5); // .5 so it doesn't blur
    [alertView addSubview:spinner];
    [spinner startAnimating];
    [alertView show];
    */
	if(!paginating){
		if(currentTextSize-25>=50){
			currentTextSize-=25;
			[self updatePagination];
			if(currentTextSize==50){
				[decTextSizeButton setEnabled:NO];
			}
			[incTextSizeButton setEnabled:YES];
		}
	}
}

- (IBAction) doneClicked:(id)sender
{
    if ([comingView isEqualToString:@"PREVIEW"])
    {
        
        WebViewController *epub=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
        
        [self presentModalViewController:epub animated:NO];
    }
    else
    {
        
        ViewController *epub=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
        
        [self presentModalViewController:epub animated:NO];
    }


}


- (IBAction) slidingStarted:(id)sender
{
    int targetPage = ((pageSlider.value/(float)100)*(float)totalPagesCount);
    if (targetPage==0) {
        targetPage++;
    }
	[currentPageLabel setText:[NSString stringWithFormat:@"%d/%d", targetPage, totalPagesCount]];
}

- (IBAction) slidingEnded:(id)sender{
	int targetPage = (int)((pageSlider.value/(float)100)*(float)totalPagesCount);
    if (targetPage==0) {
        targetPage++;
    }
	int pageSum = 0;
	int chapterIndex = 0;
	int pageIndex = 0;
	for(chapterIndex=0; chapterIndex<[loadedEpub.spineArray count]; chapterIndex++){
		pageSum+=[[loadedEpub.spineArray objectAtIndex:chapterIndex] pageCount];
//		NSLog(@"Chapter %d, targetPage: %d, pageSum: %d, pageIndex: %d", chapterIndex, targetPage, pageSum, (pageSum-targetPage));
		if(pageSum>=targetPage)
        {
			pageIndex = [[loadedEpub.spineArray objectAtIndex:chapterIndex] pageCount] - 1 - pageSum + targetPage;
			break;
		}
	}
	[self loadSpine:chapterIndex atPageIndex:pageIndex];
}

-(void)doDoubleTap
{
    NSLog(@"Double tab");
}


-(IBAction)fullscreen:(id)sender
{
    
    if ([File_Copy iSIPhone])
    {
        if ([File_Copy iSIPhone5])
        {
            if (fullscree_check==0)
            {
                webView.frame=CGRectMake(0, 50, 320, 500);
                fullscree_check=1;
                _topView.hidden=YES;
                currentPageLabel.hidden=YES;
                pageSlider.hidden=YES;
                 _titleLabelFull.hidden=NO;
                
                
                NSUserDefaults *menuUserDefaults = [NSUserDefaults standardUserDefaults];
                
                NSString *mode_str=[menuUserDefaults objectForKey:@"btnM1"];
                
                if ([mode_str isEqualToString:@"BLACK"])
                {
                    _titleLabelFull.textColor=[UIColor whiteColor];
                }
                else
                {
                     _titleLabelFull.textColor=[UIColor blackColor];
                }
                
                
                
               // _fullscreenButton.frame=CGRectMake(980,0, 44, 44);
                [_fullscreenButton setImage:[UIImage imageNamed:@"topbarclosePort.png"] forState:UIControlStateNormal];
                
            }
            else
            {
                webView.frame=CGRectMake(0, 70, 320, 441);
                currentPageLabel.hidden=NO;
                pageSlider.hidden=NO;
                 _titleLabelFull.hidden=YES;
              //  _fullscreenButton.frame=CGRectMake(270, 2, 44, 44);
                [_fullscreenButton setImage:[UIImage imageNamed:@"topbarfulPort.png"] forState:UIControlStateNormal];
                fullscree_check=0;
                _topView.hidden=NO;
                
            }

        }
        else
        {
            if (fullscree_check==0)
            {
                webView.frame=CGRectMake(0, 50, 320, 400);
                fullscree_check=1;
                _topView.hidden=YES;
                currentPageLabel.hidden=YES;
                pageSlider.hidden=YES;
                _titleLabelFull.hidden=NO;
                
                
                NSLog(@"Enter YES");
                
                
                NSUserDefaults *menuUserDefaults = [NSUserDefaults standardUserDefaults];
                
                NSString *mode_str=[menuUserDefaults objectForKey:@"btnM1"];
                
                if ([mode_str isEqualToString:@"BLACK"])
                {
                    _titleLabelFull.textColor=[UIColor whiteColor];
                }
                else
                {
                    _titleLabelFull.textColor=[UIColor blackColor];
                }
                
                
                
                // _fullscreenButton.frame=CGRectMake(980,0, 44, 44);
                [_fullscreenButton setImage:[UIImage imageNamed:@"topbarclosePort.png"] forState:UIControlStateNormal];
                
            }
            else
            {
                
                NSLog(@"Enter NO");
               webView.frame=CGRectMake(0, 50, 320, 360);
                //webView.frame=CGRectMake(0, 70, 320, 441);
                currentPageLabel.hidden=NO;
                pageSlider.hidden=NO;
                _titleLabelFull.hidden=YES;
                //  _fullscreenButton.frame=CGRectMake(270, 2, 44, 44);
                [_fullscreenButton setImage:[UIImage imageNamed:@"topbarfulPort.png"] forState:UIControlStateNormal];
                fullscree_check=0;
                _topView.hidden=NO;
                
            }
        }
        
        
        
        
       
    }
    else
    {
    
    
    if (landscape==1)
    {
        
        if (fullscree_check==0) {
            webView.frame=CGRectMake(0, 50, 1024, 768);
            fullscree_check=1;
            _topView.hidden=YES;
             _titleLabelFull.hidden=NO;
            
            NSUserDefaults *menuUserDefaults = [NSUserDefaults standardUserDefaults];
            
            NSString *mode_str=[menuUserDefaults objectForKey:@"btnM1"];
            
            if ([mode_str isEqualToString:@"BLACK"])
            {
                _titleLabelFull.textColor=[UIColor whiteColor];
            }
            else
            {
                _titleLabelFull.textColor=[UIColor blackColor];
            }
            
            
            _fullscreenButton.frame=CGRectMake(980,0, 44, 44);
            [_fullscreenButton setImage:[UIImage imageNamed:@"topbarclosePort.png"] forState:UIControlStateNormal];
            
        }
        else
        {
            webView.frame=CGRectMake(0, 70, 1024, 620);
            
             _fullscreenButton.frame=CGRectMake(950, 8, 44, 44);
           [_fullscreenButton setImage:[UIImage imageNamed:@"topbarfulPort.png"] forState:UIControlStateNormal];
            fullscree_check=0;
            _topView.hidden=NO;
             _titleLabelFull.hidden=YES;
            
        }
    }
    else
    {
    if (fullscree_check==0)
    {
        webView.frame=CGRectMake(0, 50, 768, 1004);
        
         _fullscreenButton.frame=CGRectMake(724, 0, 44, 44);
         [_fullscreenButton setImage:[UIImage imageNamed:@"topbarclosePort.png"] forState:UIControlStateNormal];
        fullscree_check=1;
        _topView.hidden=YES;
        
        NSUserDefaults *menuUserDefaults = [NSUserDefaults standardUserDefaults];
        
        NSString *mode_str=[menuUserDefaults objectForKey:@"btnM1"];
        
        if ([mode_str isEqualToString:@"BLACK"])
        {
            _titleLabelFull.textColor=[UIColor whiteColor];
        }
        else
        {
            _titleLabelFull.textColor=[UIColor blackColor];
        }
        

        
         _titleLabelFull.hidden=NO;
        
    }
    else
    {
         webView.frame=CGRectMake(0, 70, 768, 876);
_fullscreenButton.frame=CGRectMake(720, 8, 44, 44);
        [_fullscreenButton setImage:[UIImage imageNamed:@"topbarfulPort.png"] forState:UIControlStateNormal];
        fullscree_check=0;
        _topView.hidden=NO;
         _titleLabelFull.hidden=YES;
    }
    }
    }
}


- (IBAction) showChapterIndex:(id)sender{
    
    
    settingsView.hidden=YES;
    settingscheck=0;
    
    
    if ([File_Copy iSIPhone5])
    {
        
        ChapterListViewController* chapterListView = [[ChapterListViewController alloc] initWithNibName:@"ChapterListViewController~5" bundle:[NSBundle mainBundle]];
        
        [chapterListView setEpubViewController:self];
        
        [self presentViewController:chapterListView animated:YES completion:nil];
       
    }
    else
    {
        
        ChapterListViewController* chapterListView = [[ChapterListViewController alloc] initWithNibName:@"ChapterListViewController" bundle:[NSBundle mainBundle]];
        
        [chapterListView setEpubViewController:self];
        
        [self presentViewController:chapterListView animated:YES completion:nil];
    
    }

  }


- (void)webViewDidFinishLoad:(UIWebView *)theWebView{
	
    NSUserDefaults *menuUserDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *mode_str=[menuUserDefaults objectForKey:@"btnM1"];
    
    
    NSLog(@"RECEIVED");
    

    
    NSString *imgURL = [NSString stringWithFormat:@"document.elementFromPoint(%f, %f).src", 0.0, 0.0];
    NSString *urlToSave = [theWebView stringByEvaluatingJavaScriptFromString:imgURL];
    NSLog(@"urlToSave :%@",urlToSave);
    
    
    
    if([mode_str isEqualToString:@"BLACK"]){
        [webView setOpaque:NO];
        [webView setBackgroundColor:[UIColor blackColor]];
        NSString *jsString2 = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextFillColor= 'white'"];
        [webView stringByEvaluatingJavaScriptFromString:jsString2];
        self.view.backgroundColor=[UIColor blackColor];
        
    }
    else if([mode_str isEqualToString:@"WHITE"]){
        [webView setOpaque:NO];
        [webView setBackgroundColor:[UIColor whiteColor]];
        NSString *jsString2 = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextFillColor= 'black'"];
        [webView stringByEvaluatingJavaScriptFromString:jsString2];
        
        self.view.backgroundColor=[UIColor whiteColor];
        
    }
    else{
        [webView setOpaque:NO];
        [webView setBackgroundColor:[UIColor colorWithRed:243.0/255.0 green:228.0/255.0  blue:171.0/255.0  alpha:1]];
        NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextFillColor= 'black'"];
        [webView stringByEvaluatingJavaScriptFromString:jsString];
        
        self.view.backgroundColor=[UIColor colorWithRed:243.0/255.0 green:228.0/255.0  blue:171.0/255.0  alpha:1];
        
    }

    
    
	NSString *varMySheet = @"var mySheet = document.styleSheets[0];";
	
	NSString *addCSSRule =  @"function addCSSRule(selector, newRule) {"
	"if (mySheet.addRule) {"
	"mySheet.addRule(selector, newRule);"								// For Internet Explorer
	"} else {"
	"ruleIndex = mySheet.cssRules.length;"
	"mySheet.insertRule(selector + '{' + newRule + ';}', ruleIndex);"   // For Firefox, Chrome, etc.
	"}"
	"}";
	
    
    NSUserDefaults *check_lay=[NSUserDefaults standardUserDefaults];
   
    NSString *lay_STR=[check_lay objectForKey:@"LAYOUT"];
    NSString *insertRule1=@"";
    
    if ([lay_STR isEqualToString:@"DOUBLE"]) {
        insertRule1= [NSString stringWithFormat:@"addCSSRule('html', 'padding: 0px; height: %fpx; -webkit-column-gap: 0px; -webkit-column-width: %fpx;')", webView.frame.size.height, webView.frame.size.width / 2];
    }
    else
    {
         insertRule1 = [NSString stringWithFormat:@"addCSSRule('html', 'padding: 0px; height: %fpx; -webkit-column-gap: 0px; -webkit-column-width: %fpx;')", webView.frame.size.height, webView.frame.size.width];
    }
    
	
     
    /*
    NSString *insertRule1 = [NSString stringWithFormat:@"addCSSRule('html', 'padding: 0px; height: %fpx; -webkit-column-gap: 0px; -webkit-column-width: %fpx;')", webView.frame.size.height, webView.frame.size.width / 2];
     */
    
	NSString *insertRule2 = [NSString stringWithFormat:@"addCSSRule('p', 'text-align: justify;')"];
	NSString *setTextSizeRule = [NSString stringWithFormat:@"addCSSRule('body', '-webkit-text-size-adjust: %d%%;')", currentTextSize];
    
    NSUserDefaults *checkValue=[NSUserDefaults standardUserDefaults];
    
    [checkValue setObject:[NSString stringWithFormat:@"%d",currentTextSize] forKey:@"GLOBAL_SIZE"];
    
    
	NSString *setHighlightColorRule;

    if([menuUserDefaults boolForKey:@"btnM1"])
    {
       
        setHighlightColorRule = [NSString stringWithFormat:@"addCSSRule('highlight', 'background-color: yellow;')"];
        
    }
    
    else{
        
        setHighlightColorRule = [NSString stringWithFormat:@"addCSSRule('highlight', 'background-color: gray;')"];
       
    }

    
//    NSString *setImageRule = [NSString stringWithFormat:@"addCSSRule('img', 'max-width: %fpx; height:%fpx;')", webView.frame.size.width *0.75,webView.frame.size.height * 0.5];
//    
//    [webView stringByEvaluatingJavaScriptFromString:setImageRule];
    
    
 // NSString *setImageRule =  [NSString stringWithFormat:@"addCSSRule('img', 'max-width: %fpx; height: %fpx;')",50.00,50.00]; [webView stringByEvaluatingJavaScriptFromString:setImageRule];
    
    
    
	
	[webView stringByEvaluatingJavaScriptFromString:varMySheet];
	
	[webView stringByEvaluatingJavaScriptFromString:addCSSRule];
		
	[webView stringByEvaluatingJavaScriptFromString:insertRule1];
	
	[webView stringByEvaluatingJavaScriptFromString:insertRule2];
	
	[webView stringByEvaluatingJavaScriptFromString:setTextSizeRule];
	
	[webView stringByEvaluatingJavaScriptFromString:setHighlightColorRule];
	
    
    /*
    
   NSString *setImageRule11= [NSString stringWithFormat:@"addCSSRule('img', 'max-width: %fpx; height: %fpx;')", self.webView.frame.size.width*0.65,self.webView.frame.size.height*0.65];
    [webView stringByEvaluatingJavaScriptFromString:setImageRule11];
    
    */
    
    
    
    
    
    
	if(currentSearchResult!=nil){
	//	NSLog(@"Highlighting %@", currentSearchResult.originatingQuery);
        [webView highlightAllOccurencesOfString:currentSearchResult.originatingQuery];
	}
	
	
	int totalWidth = [[webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.scrollWidth"] intValue];
	pagesInCurrentSpineCount = (int)((float)totalWidth/webView.bounds.size.width);
	
	[self gotoPageInCurrentSpine:currentPageInSpineIndex];
    
    
   // NSLog(@"HELLO:%@",webView.subviews);
    
    
    
   NSString *jsonString = [webView stringByEvaluatingJavaScriptFromString:@"(function(fields) { var O=[]; for(var i=0; i<fields.length;i++) {O.push(fields[i].value);} return JSON.stringify(O); })(document.querySelectorAll('input[type=\"text\"]'))"];
    
    NSLog(@"HELLO VIEW:%@",jsonString);
    
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType

{
    
    NSURL* url = [request  URL];
    
    NSString *urlImage=[NSString stringWithFormat:@"%@",url];
    
    if ([urlImage rangeOfString:@".jpg"].location == NSNotFound||[urlImage rangeOfString:@".png"].location == NSNotFound)
    {
        NSLog(@"");
    } else
    {
        NSLog(@"string contains bla! fhfhfhfh");
    }
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        
        //path to called chapter
        NSURL* url = [request  URL];
        
        NSString * newChapterURL = [url path];
        
        //get chapter index by chapter path
        int indexNewChapter = -1;
        NSString *path;
        
        for (int i=0; i<loadedEpub.spineArray.count & indexNewChapter == -1; i++) {
            
            path = ((Chapter*)[loadedEpub.spineArray objectAtIndex:i]).spinePath;
            
            if([path isEqualToString:newChapterURL]){
                indexNewChapter = i;
            }
        }
        [self loadSpine:indexNewChapter atPageIndex:0];
        
        
        currentSpineIndex=indexNewChapter;
        
        return NO;
    }
    
    return YES;
}


- (void) updatePagination{
	if(epubLoaded){
        if(!paginating){
            NSLog(@"Pagination Started!");
            paginating = YES;
            totalPagesCount=0;
            [self loadSpine:currentSpineIndex atPageIndex:currentPageInSpineIndex];
            [[loadedEpub.spineArray objectAtIndex:0] setDelegate:self];
            [[loadedEpub.spineArray objectAtIndex:0] loadChapterWithWindowSize:webView.bounds fontPercentSize:currentTextSize];
            [currentPageLabel setText:@"?/?"];
        }
	}
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
	if(searchResultsPopover==nil){
		searchResultsPopover = [[UIPopoverController alloc] initWithContentViewController:searchResViewController];
		[searchResultsPopover setPopoverContentSize:CGSizeMake(400, 600)];
	}
	if (![searchResultsPopover isPopoverVisible]) {
		[searchResultsPopover presentPopoverFromRect:searchBar.bounds inView:searchBar permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
	}
//	NSLog(@"Searching for %@", [searchBar text]);
	if(!searching){
		searching = YES;
		[searchResViewController searchString:[searchBar text]];
        [searchBar resignFirstResponder];
	}
}


#pragma mark -
#pragma mark Rotation support

// Ensure that the view controller supports rotation and that the split view can therefore show in both portrait and landscape.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    NSLog(@"shouldAutorotate");
    [self updatePagination];
	return YES;
}



#pragma mark -
#pragma mark View lifecycle



- (void)defineSelection:(id)sender
{
    NSString *selection = [webView stringByEvaluatingJavaScriptFromString:@"window.getSelection().toString()"];
    
    
    initialtext=selection;
    
    NSLog(@"Share:%@",initialtext);
    
   
    
    NSUserDefaults *textini=[NSUserDefaults standardUserDefaults];
    [textini setObject:initialtext forKey:@"SHARE"];
    
    
    NSString *actionSheetTitle = selection; //Action Sheet Title
    //NSString *destructiveTitle = @"Destructive Button"; //Action Sheet Button Titles
    NSString *other1 = @"Share via Facebook";
    NSString *other2 = @"Share via Twitter";
    NSString *cancelTitle = @"Cancel";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2, nil];
    [actionSheet showInView:self.view];
    
    
    // Do something with the selection
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"Initial:%@",initialtext);
    
    NSLog(@"EPUB NAME:%@",epubName);
    
    NSString *share_url=[NSString stringWithFormat:@"http://books.vikatan.com/index.php?bid=%@",epubName];

    NSString *ImageURL = [NSString stringWithFormat:@"http://books.vikatan.com/files/medium/%@.jpg",epubName];;
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:ImageURL]];
    NSUserDefaults *textini=[NSUserDefaults standardUserDefaults];
    //[textini setObject:textini forKey:@"SHARE"];
    
    
    NSString *shareStr=[textini objectForKey:@"SHARE"];
    
    if (buttonIndex==0)
    {
        NSLog(@"IND1");
        
//        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
//        {
            SLComposeViewController *tweetSheet = [SLComposeViewController
                                                   composeViewControllerForServiceType:SLServiceTypeFacebook];
            [tweetSheet setInitialText:shareStr];
            [tweetSheet addImage:[UIImage imageWithData:imageData]];
            [tweetSheet addURL:[NSURL URLWithString:share_url]];
           
            [self presentViewController:tweetSheet animated:YES completion:nil];
      //  }
       
        
      
    }
    else if (buttonIndex==1)
    {
        NSLog(@"IND2");
        
        
        if (shareStr.length>140)
        {
            
           NSString *mystr=[shareStr substringToIndex:100];
            
            NSString *new_str=[NSString stringWithFormat:@"%@...",mystr];
            
//            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
//            {
                SLComposeViewController *tweetSheet = [SLComposeViewController
                                                       composeViewControllerForServiceType:SLServiceTypeTwitter];
                [tweetSheet setInitialText:new_str];
                [tweetSheet addURL:[NSURL URLWithString:share_url]];
                [self presentViewController:tweetSheet animated:YES completion:nil];
          //  }

        }
        else
        {
//        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
//        {
            SLComposeViewController *tweetSheet = [SLComposeViewController
                                                   composeViewControllerForServiceType:SLServiceTypeTwitter];
            [tweetSheet setInitialText:shareStr];
             [tweetSheet addURL:[NSURL URLWithString:share_url]];
            [self presentViewController:tweetSheet animated:YES completion:nil];
      //  }
        }
        
    }
  
}


- (IBAction) sliderValueChanged:(UISlider *)sender
{
    
    float center=sender.value;
    
    [[UIScreen mainScreen] setBrightness:center];
}

-(void)doSingleTap
{
    settingscheck=0;
    settingsView.hidden=YES;
}
- (NSString *) applicationDocumentsDirectory
{
    return [NSString stringWithFormat: @"%@",
            [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex: 0]];
}
- (void)loadChapters
{
   
    
    NSString *cachePath = [NSString stringWithFormat:@"%@/Cache", [self applicationDocumentsDirectory]];
    int ret1 = [EPubONI initEPubEnv:cachePath];
    if (ret1 == AFD_EPUB_OK)
    {
        NSLog(@"Success");
    }
    else
        NSLog(@"FAiled");
    
    NSUserDefaults *passValues=[NSUserDefaults standardUserDefaults];
    //[passValues setObject:idArray[indexPath.row] forKey:@"BOOK_ID"];
    // [passValues setObject:epubArray[indexPath.row] forKey:@"BOOK_EPUB"];
    // [passValues setObject:textArray[indexPath.row] forKey:@"BOOK_NAME"];
    
    
    NSString *epubName=[NSString stringWithFormat:@"%@savedEpub.epub",[passValues objectForKey:@"BOOK_EPUB"]];
    
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSArray *filePathsArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:documentsDirectory  error:nil];
    //NSLog(@"files array %@", filePathsArray);
    
    NSString *data_path=[passValues objectForKey:@"BOOK_EPUB"];
    
    
    
    
    NSString *compressed_data_path=[NSString stringWithFormat:@"%@/%@",documentsDirectory,epubName];
    
    // NSString *compressed_data_path=[NSString stringWithFormat:@"%@/%@.epub",documentsDirectory,@"savedEpub"];
    
    // savedEpub.epub
    
    NSLog(@"Compressed datapath:%@",compressed_data_path);
    
    
    
    
  NSMutableArray  *array_list = [[NSMutableArray alloc] init];
    
    //    NSString *bundlePath = [[NSBundle mainBundle] resourcePath];
    //    NSString *epubFilePath = [bundlePath stringByAppendingString:epubName];
    //    NSLog(@"epubFilePath:%@",epubFilePath);
    //
    
    
    
    
    int ret = [EPubONI openEPubBook:compressed_data_path];
    if (ret > 0) {
        NSMutableArray *tmp = [[NSMutableArray alloc] init];
        [EPubONI getEPubChapter:tmp Handle:ret];
        int count = 0, count2 = 0;
        for (int i = 0; i < [tmp count]; i++) {
            EPubChapter *chapter = [tmp objectAtIndex:i];
            if (chapter.level == 1) {
                count ++;
                [array_list addObject:[NSString stringWithFormat:@"%d.%@",count,  chapter.title]];
                count2 = 0;
            }
            else if (chapter.level == 2) {
                count2++;
                [array_list addObject:[NSString stringWithFormat:@"    %d.%@",count2,  chapter.title]];
            }
        }
    }
    else
    {
        // [array_list addObject:[NSString stringWithFormat:@"error code is %d", ret]];
        [array_list addObject:[NSString stringWithFormat:@"Chapter 1"]];
        
    }
    
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setObject:array_list forKey:@"CHAPTERS"];
    
    
    // reloadData];
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

 // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [self.view addSubview:settingsView];
     [self.view addSubview:bookmarkView];
    
    bookmarkView.hidden=YES;
    settingsView.hidden=YES;
    settingscheck=0;
    
    UITapGestureRecognizer *singleTap = [[[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(doSingleTap)] autorelease];
    singleTap.numberOfTapsRequired = 1;
    singleTap.delegate=self;
    [settingsView addGestureRecognizer:singleTap];
    
    [[UIScreen mainScreen] setBrightness:1.0];
    
    
    if ([comingView isEqualToString:@"PREVIEW"]) {
        viewbookmarkBtn.enabled=NO;
        _addBookmarkbutton.enabled=NO;
       
        [_doneButton setBackgroundImage:[UIImage imageNamed:@"storesLands.png"] forState:UIControlStateNormal];
        [_doneButton setTitle:@"Back" forState:UIControlStateNormal];
        [_doneButton setImage:nil forState:UIControlStateNormal];
        
    }
    else
    {
        UIMenuItem *defineItem = [[[UIMenuItem alloc] initWithTitle:@"Share" action:@selector(defineSelection:)] autorelease];
        [[UIMenuController sharedMenuController] setMenuItems:[NSArray arrayWithObject:defineItem]];
        
        [_doneButton setImage:[UIImage imageNamed:@"library.png"] forState:UIControlStateNormal];
    }
    

    
    
   //  [_view setBrightness:0.5];
    
     
    
    
    NSUserDefaults *check_lay=[NSUserDefaults standardUserDefaults];
    
    NSString *lay_STR=[check_lay objectForKey:@"LAYOUT"];
    
    
    settingsView.hidden=YES;
    settingscheck=0;
    
    if ([lay_STR isEqualToString:@"DOUBLE"]) {
        
        _segmentedControl.selectedSegmentIndex=1;
        
    }
    else
    {
          _segmentedControl.selectedSegmentIndex=0;
    }
    
    
    NSUserDefaults *passValues=[NSUserDefaults standardUserDefaults];
      

    
    _titleLabel.text=[NSString stringWithFormat:@"%@",[passValues objectForKey:@"BOOK_NAME"]];
    
    _titleLabelFull.text=[NSString stringWithFormat:@"%@",[passValues objectForKey:@"BOOK_NAME"]];
    _titleLabelFull.hidden=YES;
    
    /*
    alertView = [[UIAlertView alloc] initWithTitle:@"Loading..."
                                           message:@"\n"
                                          delegate:self
                                 cancelButtonTitle:nil
                                 otherButtonTitles:nil];
    
    
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.center = CGPointMake(139.5, 75.5); // .5 so it doesn't blur
    [alertView addSubview:spinner];
    [spinner startAnimating];
    [alertView show];
    */
    
    for (UIView *subview in _searchBar.subviews) {
        if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
            [subview removeFromSuperview];
            break;
        }
    }
    
    //tapGestureRecognizer.delegate = self;
    
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
     NSString *data_path=epubName;
     NSString *saved_Epub=[NSString stringWithFormat:@"%@savedEpub.epub",epubName];
    
    NSString* foofile = [documentsDirectory stringByAppendingPathComponent:saved_Epub];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:foofile];
    
    
   // NSLog(@"files array %@", filePathsArray);
    
   
    
    
     
    
    NSString *compressed_data_path=[NSString stringWithFormat:@"%@/%@.epub",documentsDirectory,data_path];
    
    NSLog(@"Compressed datapath:%@",compressed_data_path);
    
    
    
    if (![comingView isEqualToString:@"PREVIEW"])
    {

    
        
        NSUserDefaults *use=[NSUserDefaults standardUserDefaults];
        //[use setObject:userName forKey:@"DISPLAY_NAME"];
        
        NSString *user1Name=[use objectForKey:@"DISPLAY_NAME"];
        
        if (![user1Name isEqualToString:@""])
        {
            
            
            
            self.trackedViewName = [NSString stringWithFormat:@"%@-BookId:%@:Preview Book",user1Name,epubName];;
            
            
            
        }
        else
        {
            
             self.trackedViewName = [NSString stringWithFormat:@"Guest User-BookId:%@:Preview Book",epubName];;
            
        }
        

        
        
        
        
        
        
        
        
        
        if (fileExists)
        {
            NSLog(@"FILE EXISTS YES:%@",saved_Epub);
            
            NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:saved_Epub];
            
            
            
            @try {
                 [self loadEpub:[NSURL fileURLWithPath:savedImagePath]];
            }
            @catch (NSException *exception)
            {
                NSLog(@"EPUB CATCHED");
//                @try
//                {
//                    [self loadEpub:[NSURL fileURLWithPath:compressed_data_path]];
//                }
//                @catch (NSException *exception)
//                {
//                }
            }
         
           
            
            NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
            
            [check setObject:@"ORIGINAL" forKey:@"VIEW"];
        }
        else
        {
            
            
            NSData * fileData = [NSData dataWithContentsOfFile: compressed_data_path];
            
            NSMutableArray *strings=[[NSMutableArray alloc]init];
            
            
            NSMutableString *result1 = [NSMutableString string];
            const char *bytes = [fileData bytes];
            for (int i = 0; i < [fileData length]; i++)
            {
        
                
                
                
                
                
                
                
                
                [result1 appendFormat:@"%02hhx", (unsigned char)bytes[i]];
                
                NSString *str=[NSString stringWithFormat:@"%hhu",(unsigned char)bytes[i]];
             
            
                if ((i>=100&&i<=116)||(i>=217&&i<=233)||(i>=334&&i<=350)||(i>=451&&i<=467)||(i>=568&&i<=584))
                {
                   // NSLog(@"HJHJ:%@",str);
//                    
//                    NSLog(@"PRINT I:%d",i);
//                    
//                     NSLog(@"BYTES:%s",bytes);
                }
                else
                {
                           //  NSLog(@"YSYS:%@",str);
                    [strings addObject:str];
                    
                    
                }
            }
            
            
              //NSLog(@"ARRAY:%@",strings);
            
            
            unsigned c = strings.count;
            uint8_t *bytes_new = malloc(sizeof(*bytes) * c);
            
            unsigned i;
            for (i = 0; i < c; i++)
            {
                NSString *str = [strings objectAtIndex:i];
                int byte = [str intValue];
                bytes_new[i] = (uint8_t)byte;
            }
            
            NSData *imageData = [NSData dataWithBytesNoCopy:bytes_new length:c freeWhenDone:YES];
                       
            
            NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:saved_Epub];
            
            
            NSLog(@"DOC YYYYYY>>>>:%@",saved_Epub);
            
            
            [imageData writeToFile:savedImagePath atomically:NO];
            
            @try
            {
                [self loadEpub:[NSURL fileURLWithPath:savedImagePath]];
            }
            @catch (NSException *exception)
            {
                NSLog(@"EPUB CATCHED");
//                @try
//                {
//                    [self loadEpub:[NSURL fileURLWithPath:compressed_data_path]];
//                }
//                @catch (NSException *exception)
//                {
//                }

            }

            
            NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
            
            [check setObject:@"ORIGINAL" forKey:@"VIEW"];
            
            
        }

        
    }
    else
    {
        NSUserDefaults *use=[NSUserDefaults standardUserDefaults];
        //[use setObject:userName forKey:@"DISPLAY_NAME"];
        
        NSString *user1Name=[use objectForKey:@"DISPLAY_NAME"];
        
        if (![user1Name isEqualToString:@""])
        {
            
            
            
            self.trackedViewName = [NSString stringWithFormat:@"%@-BookId:%@:Reader",user1Name,epubName];;
            
            
            
        }
        else
        {
            
            self.trackedViewName = [NSString stringWithFormat:@"Guest User-BookId:%@:Reader",epubName];;
            
        }

    }
    
    

    
    
    
    
    
    if ([comingView isEqualToString:@"PREVIEW"])
    {
        [self loadEpub:[NSURL fileURLWithPath:compressed_data_path]];
        
        NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
        
        [check setObject:@"PREVIEW" forKey:@"VIEW"];
        
    }
  
    
    
	[webView setDelegate:self];
    
    
    NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil];
    [NSURLCache setSharedURLCache:sharedCache];
    [sharedCache release];
    sharedCache = nil;
    
    
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    
    fullscree_check=0;
   
    if ([File_Copy iSIPhone])
    {
        if ([File_Copy iSIPhone5])
        {
                            webView.frame=CGRectMake(0, 50, 320, 450);
               // fullscree_check=1;
                //_topView.hidden=YES;
                
               // _fullscreenButton.frame=CGRectMake(980,0, 44, 44);
               // [_fullscreenButton setImage:[UIImage imageNamed:@"topbarclosePort.png"] forState:UIControlStateNormal];
                
           
        }
        else
        {
            
            NSLog(@"HELLO");
            
             webView.frame=CGRectMake(0, 50, 320, 360);
        }
    }
    else
    {
        webView.frame=CGRectMake(0, 70, 768, 870);
    }
    
    
    
		[_fullscreenButton setImage:[UIImage imageNamed:@"topbarfulPort.png"] forState:UIControlStateNormal];
    
    
	UIScrollView* sv = nil;
	for (UIView* v in  webView.subviews) {
		if([v isKindOfClass:[UIScrollView class]]){
			sv = (UIScrollView*) v;
			sv.scrollEnabled = NO;
			sv.bounces = NO;
		}
	}
    
    
    NSUserDefaults *checkValue=[NSUserDefaults standardUserDefaults];
    
    currentTextSize=[[checkValue objectForKey:@"GLOBAL_SIZE"]intValue];
    
    NSLog(@"GLOBAL SIZE:%d",currentTextSize);
    
    if (currentTextSize==[NSNull null]||currentTextSize==0)
    {
        
         NSLog(@"GLOBAL SIZE:%d",currentTextSize);
        
        currentTextSize = 100;
    }
     
		 
	
	UISwipeGestureRecognizer* rightSwipeRecognizer = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gotoNextPage)] autorelease];
	[rightSwipeRecognizer setDirection:UISwipeGestureRecognizerDirectionLeft];
	
	UISwipeGestureRecognizer* leftSwipeRecognizer = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gotoPrevPage)] autorelease];
	[leftSwipeRecognizer setDirection:UISwipeGestureRecognizerDirectionRight];
	
	[webView addGestureRecognizer:rightSwipeRecognizer];
	[webView addGestureRecognizer:leftSwipeRecognizer];
	
	[pageSlider setThumbImage:[UIImage imageNamed:@"slider_ball.png"] forState:UIControlStateNormal];
	[pageSlider setMinimumTrackImage:[[UIImage imageNamed:@"orangeSlide.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:0] forState:UIControlStateNormal];
	[pageSlider setMaximumTrackImage:[[UIImage imageNamed:@"yellowSlide.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:0] forState:UIControlStateNormal];
    
	searchResViewController = [[SearchResultsViewController alloc] initWithNibName:@"SearchResultsViewController" bundle:[NSBundle mainBundle]];
	searchResViewController.epubViewController = self;
    
    /*
    UIScrollView* sv = nil;
    for (UIView* v in webView.subviews) {
        if([v isKindOfClass:[UIScrollView class]]){
            sv = (UIScrollView*) v;
            sv.scrollEnabled = YES;
            sv.showsHorizontalScrollIndicator = NO;
            sv.showsVerticalScrollIndicator = NO;
            sv.bounces = NO;
            sv.delegate = self;
        }
    }
     */
    
    [self loadChapters];
}

- (void)viewDidUnload {
    [self setBookmarkTable:nil];
    [self setBookmarkView:nil];
    [self setViewbookmarkBtn:nil];
	self.toolbar = nil;
	self.webView = nil;
	self.chapterListButton = nil;
	self.decTextSizeButton = nil;
	self.incTextSizeButton = nil;
	self.pageSlider = nil;
	self.currentPageLabel = nil;	
}

#pragma mark -
#pragma mark Memory management

/*
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
*/

- (void)dealloc {
    self.toolbar = nil;
	self.webView = nil;
	self.chapterListButton = nil;
	self.decTextSizeButton = nil;
	self.incTextSizeButton = nil;
	self.pageSlider = nil;
	self.currentPageLabel = nil;
	[loadedEpub release];
	[chaptersPopover release];
	[searchResultsPopover release];
	[searchResViewController release];
	[currentSearchResult release];
    [viewbookmarkBtn release];
    [bookmarkView release];
    [bookmarkTable release];
    [super dealloc];
}

-(IBAction)dark:(id)sender
{
    NSLog(@"Dark");
    
    NSUserDefaults *userDefaults2 = [NSUserDefaults standardUserDefaults];
    [userDefaults2 setObject:@"BLACK" forKey:@"btnM1"];
    [userDefaults2 synchronize];
    
    [webView setOpaque:NO];
    [webView setBackgroundColor:[UIColor blackColor]];
    NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextFillColor= 'white'"];
    [webView stringByEvaluatingJavaScriptFromString:jsString];
    
    
    self.view.backgroundColor=[UIColor blackColor];
    
}
-(IBAction)light:(id)sender
{
   NSLog(@"Light");
    
    NSUserDefaults *userDefaults2 = [NSUserDefaults standardUserDefaults];
   [userDefaults2 setObject:@"WHITE" forKey:@"btnM1"];
    [userDefaults2 synchronize];
    
    [webView setOpaque:NO];
    [webView setBackgroundColor:[UIColor whiteColor]];
    NSString *jsString2 = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextFillColor= 'black'"];
    [webView stringByEvaluatingJavaScriptFromString:jsString2];
    
     self.view.backgroundColor=[UIColor whiteColor];

}


-(IBAction)gray:(id)sender
{
    NSLog(@"gray");
    
    NSUserDefaults *userDefaults2 = [NSUserDefaults standardUserDefaults];
    [userDefaults2 setObject:@"GRAY" forKey:@"btnM1"];
    
    [userDefaults2 synchronize];
    
    [webView setOpaque:NO];
    [webView setBackgroundColor:[UIColor colorWithRed:243.0/255.0 green:228.0/255.0  blue:171.0/255.0  alpha:1]];
    NSString *jsString2 = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextFillColor= 'black'"];
    [webView stringByEvaluatingJavaScriptFromString:jsString2];
    
     self.view.backgroundColor=[UIColor colorWithRed:243.0/255.0 green:228.0/255.0  blue:171.0/255.0  alpha:1];

}

-(BOOL)shouldAutorotate{
    return YES;
}


-(NSInteger)supportedInterfaceOrientations{
    
   // [[_searchBar.subviews objectAtIndex:0] removeFromSuperview];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        
    }
    else
    {
    
    
    _searchBar.backgroundColor=[UIColor clearColor];
    
    if (UIDeviceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation]))
    {
        
        _titleLabel.hidden=NO;
        
        _titleLabel.frame=CGRectMake(215, 8, 110, 44);

         webView.frame=CGRectMake(0, 70, 768, 870);
        _topView.frame=CGRectMake(0, 0, 768, 63);
        _topImage.frame=CGRectMake(0, 0, 768, 63);
        _topImage.image=[UIImage imageNamed:@"topbarnavPort.png"];
        
        
        _searchBar.frame=CGRectMake(320, 8, 200, 44);
        
        chapterListButton.frame=CGRectMake(520, 8, 44, 44);
        
        _addBookmarkbutton.frame=CGRectMake(570, 8, 44, 44);
        
        viewbookmarkBtn.frame=CGRectMake(620, 8, 44, 44);
        
        
        settingBtn.frame=CGRectMake(670, 8, 44, 44);
        
       // _fullscreenButton.frame=CGRectMake(720, 8, 44, 44);
        
        
        _titleLabelFull.frame=CGRectMake(175, 15, 429, 21);
        
       // _titleLabel.hidden=YES;
        
        
        
        fullscree_check=0;
        landscape=0;
    }
    
    else
    {
        
         _titleLabelFull.frame=CGRectMake(300, 15, 429, 21);
        _titleLabel.hidden=NO;
        
         _titleLabel.frame=CGRectMake(225, 8, 200, 44);
        
        
        _searchBar.frame=CGRectMake(445, 8, 300, 44);
        
        chapterListButton.frame=CGRectMake(750, 8, 44, 44);
        
        _addBookmarkbutton.frame=CGRectMake(800, 8, 44, 44);
        
        viewbookmarkBtn.frame=CGRectMake(850, 8, 44, 44);
        
        
        settingBtn.frame=CGRectMake(900, 8, 44, 44);
        
      //  _fullscreenButton.frame=CGRectMake(950, 8, 44, 44);

        
         webView.frame=CGRectMake(0, 70, 1024, 610);
        _topView.frame=CGRectMake(0, 0, 1024, 63);
        _topImage.frame=CGRectMake(0, 0, 1024, 63);
        _topImage.image=[UIImage imageNamed:@"topbarnavLands.png"];
        
        fullscree_check=0;
        landscape=1;
    }
        
    }
    return UIInterfaceOrientationMaskAll;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)orientation
                                duration:(NSTimeInterval)duration
{
   
    if (UIDeviceOrientationIsPortrait(orientation)) {
       
    }
    
    else {
        _searchBar.frame=CGRectMake(445, 10, 260, 44);
    }
}

@end
