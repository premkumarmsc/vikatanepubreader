/*
 * $Author: kidbaw $
 * $Revision: 59 $
 * $Date: 2012-03-23 22:44:48 +0700 (Fri, 23 Mar 2012) $
 */

#import <Foundation/Foundation.h>
#import "CoreLocation/CoreLocation.h"

typedef enum {
	enumImageScalingType_Invalid,
    enumImageScalingType_Left,
    enumImageScalingType_Top,
    enumImageScalingType_Right,
    enumImageScalingType_Bottom
} enumImageScalingType;

@interface UIImage (Extras)
- (UIImage *)imageByScalingToSize:(CGSize)targetSize;
- (UIImage *)imageByScalingToSize:(CGSize)size withOption:(enumImageScalingType)type;
- (UIImage *)imageByScalingProportionallyToSize:(CGSize)targetSize;
@end;


typedef enum {
    enumNSStringExtrasCheckingType_Invalid,
    enumNSStringExtrasCheckingType_KardBuilder_KardName,
    enumNSStringExtrasCheckingType_KardBuilder_ProfileName,
    enumNSStringExtrasCheckingType_KardBuilder_Profile_FirstName,
    enumNSStringExtrasCheckingType_KardBuilder_Profile_LastName
}enumNSStringExtrasCheckingType;

@interface NSString (Extras)
- (BOOL)isAvailableWithCheckingType:(enumNSStringExtrasCheckingType)checkingType;
@end;

@interface SupportFunction : NSObject
+ (BOOL)deviceIsIPad;
+ (int)getDeviceHeight;
+ (int)getDeviceWidth;
+ (int)getKeyboardHeight;
+ (int)getKeyboardWidth;
+ (NSMutableArray *)sortArray:(NSMutableArray *)source withKey:(NSString *)key;

+ (NSString *)getStringDistanceFromMeter:(double)meters;
+ (NSString *)getStringDistanceFromLocation:(CLLocation *)fromLoc toLocation:(CLLocation *)toLoc;
+ (NSString *)getStringDistanceFromLocation:(CLLocation *)fromLoc;

+ (NSMutableArray *)getCurrentNodeFromArray:(NSMutableArray *)array;
+ (NSMutableArray *)getCurrentCategoryFromArray:(NSMutableArray *)array;

+ (void)alertWithError:(int)error andMessage:(NSString *)message;

+ (NSDate *)dateFromRFC1123String:(NSString *)string;
+ (NSString *)stringFromDate:(NSDate *)date;
+ (NSString *)stringFromDateUseForName:(NSDate *)date;
+ (NSMutableDictionary *)repairingDictionaryWith:(NSDictionary *)dictionary;

void ALERT(NSString* title, NSString* message);
void alertView(NSString *title, NSString *message,NSString *dismissString);
UIAlertView* UI_ALERT_VIEW(NSString* title, NSString* msg, id delegate, NSString* other_button, NSString* cancel_button);

+ (void)scalePickerView:(UIPickerView *)picker toSize:(CGSize)size;

+ (UIColor *)colorFromHexString:(NSString *)hexString;

@end
